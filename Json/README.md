# M3202

## Module_Json

### Description

Ce module contient les fichiers de données de chaque exercice sous le format JSON ainsi qu'un script Python contenant diverses méthode afin d'extraire les données des fichiers JSON ci-dessus.
Ces archives JSON sont structurées de la façon suivante :

* * __**"iter"**__

* * * *Description*

Une première valeur spécifie aux algorithme le nombre d'éxecution que nécessite cet exercice nommé "iter".

* * * *Exemple*

```json
"iter":1
```

Ceci indiquera que nous effectuerons cela une seule et unique fois.

* * __**"var"**__

* * * *Description*

La seconde valeur est une liste et contient les chaînes de caractères correspondant aux états que prendre une valeur dans l'exercice en question. Celle-ci est nommée "var".

* * * *Exemple*

```json
"var":
[
    "var1",
    "var9"
]
```

Ceci indiquera que les états possibles au sein de cet exercice sont "var1" et "var9".

* * __**"init"**__

* * * *Description*

Notre troisième valeur est un dictionnaire nommé "init" relatif à l'initialisation de la valeurs pour chaque itération :

* * * __"div"__

* * * * *Description*

La première valeur de ce dictionnaire est nommée "div" et indique pour quel intervalle une valeur est valable.

* * * * *Exemple*

```json
"div":1
```

Ceci indiquera que pour chaque itération on aura un index d'initialisation dédié.

* * * __"index"__

* * * * *Description*

La seconde valeur nommée index est une liste contenant les valeur d'initialisation ordonnée dans l'ordre afin de pouvoir les affecter à une itération précise.

* * * * *Exemple*

```json
"index":
[
    "var9"
]
```

Ceci indiquera que on initialisera notre itération avec "var9".

* * * *Exemple*

```json
"init":
{
    "div":10,
    "index":
    [
        "var9",
        "var1"
    ]
}
```

Ceci indiquera que pour les 10 premières itérations on initialisera à l'état "var9" et pour les 10 suivant on intialisera à "var1".

* * __**"proba"**__

* * * *Description*

La quatrième valeur nommée "proba" est la plus imporante car celle-ci contient les arbres de possibilités pour chaque état vers n'importe quel état avec la probabilité d'y aller, celle-ci est composé de x index, ceci sont en fait les états au sein de "var". Un arbre type se compose comme il suit :

* * * __"divi"__

* * * * *Description*

Il possède en premier lieu une variable "divi" qui indique quel est le dénominateur à appliquer sur les valeurs ci-dessous.

* * * * *Exemple*

```json
"divi":100
```

Ceci indiquera que les probabilitées de cet arbre seront en pourcentage (car divisées par 100).

* * * __Les autres valeurs__

* * * * *Description*

Toutes les autres valeurs sont les autres états possibles ainsi que la probabilité d'y accéder depuis l'état présent (étant l'index de l'arbre que l'on consulte).

* * * * *Exemple*

```json
"var1":85
```

```json
"var9":15
```

Ceci indiquera que depuis l'état où nous sommes il y aura 85 chances sur le nombre spécifié dans "divi" de prendre l'état "var1" et selon 15 chances sur divi de prendre l'état "var9".

* * * *Exemple*

```json
"proba":
{
    "var1":
    {
        "divi":5,
        "var1":3,
        "var9":2
    },
    "var9":
    {
        "divi":8,
        "var1":6,
        "var9":2
    }
}
```

Ceci indiquera que si on est placé à l'état "var1", on a 3 chances sur 5 de le conserver et 2 chances sur 5 de passer à l'état "var9", tandis que si on est placé à l'état "var9", on a 2 chances sur 8 de le conserver et 6 chances sur 8 de prendre l'état "var1".

### Authors

Féry Mathieu (aka Mathius)
