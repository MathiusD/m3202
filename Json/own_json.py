import json, os

"""
    Fonction read_and_verify_json_mod
    %       Fonction qui permet de récupérer les données présente au sein
            d'une archive JSON, en vérifiant préallablement son intégrité
            dans le cas contraire, la donnée renvoyée sera None.
            Cette fonction n'attend pas un path mais une chaine de
            caractère de type : "C1" si on souhaite ouvrir depuis la racine
            le fichier suivant : "Json/C1.json".
    %IN     path_relatif : Chaine de caractère spécifiant le nom du fichier,
            dict_tab_init : Booléen indiquant si on souhaite passer le dictionnaire
                            de l'index "init" en un tableau
    %OUT    files : Dictionnaire contenu au sein du JSON (Ou None en cas de
                    fichier invalide)
"""
def read_and_verify_json_mod(path_relatif, dict_tab_init = True):
    path = autocompletion_path(path_relatif)
    if True == verify_json(path):
        files = read_json(path)
        if dict_tab_init == True:
            files['init'] = dict_to_tab(files['init'], files['iter'])
    else:
        files = None
    return files

"""
    Fonction read_json_mod
    %       Fonction qui permet de récupérer les données présente au sein
            d'une archive JSON en ne spécifiant que "C1" si on souhaite
            ouvrir depuis la racine le fichier suivant : "Json/C1.json".
    %IN     path_relatif : Chaine de caractère spécifiant le nom du fichier
    %OUT    files : Dictionnaire contenu au sein du JSON
"""
def read_json_mod(path_relatif):
    path = autocompletion_path(path_relatif)
    files = read_json_(path)
    return files

"""
    Fonction read_json
    %       Fonction qui permet de récupérer les données présente au sein
            d'une archive JSON et les renvoie sous forme de dictionnaire.
    %IN     path : Chaine de caractère spécifiant le nom du fichier
    %OUT    files : Dictionnaire contenu au sein du JSON
"""
def read_json(path):
    files_brut = open(path, 'r')
    files = json.load(files_brut)
    return files

"""
    Fonction verify_json
    %       Fonction qui permet de vérifier si un fichier situé à l'addresse
            pointé par le path en argument est un fichier JSON valide ou non
            en ne spécifiant que "C1" si on souhaite vérifier depuis la racine
            le fichier suivant : "Json/C1.json".
    %IN     path_relatif : Chaine de caractère spécifiant le nom du fichier
    %OUT    confirm : Booléen qui confirme si le fichier est un JSON valide
"""
def verify_json_mod(path_relatif):
    path = autocompletion_path(path_relatif)
    confirm = verify_json(path)
    return confirm

"""
    Fonction verify_json
    %       Fonction qui permet de vérifier si un fichier situé à l'addresse
            pointé par le path en argument est un fichier JSON valide ou non
    %IN     path : Chaine de caractère spécifiant le nom du fichier
    %OUT    confirm : Booléen qui confirme si le fichier est un JSON valide
"""
def verify_json(path):
    confirm = False
    if os.path.exists(path) ==  True:
        if os.path.isfile(path) == True:
            if path.split('.')[len(path.split('.')) - 1] == "json":
                json_parse = False
                try:
                    json.load(open(path, 'r'))
                    json_parse = True
                finally:
                    if json_parse == True:
                        confirm = True
    return confirm

"""
    Fonction autocompletion_path
    %       Fonction qui renvoie la chaine de caractère passé en
            argument avec en préfixe 'Json/' et en suffixe '.json'
    %IN     path : Chaine de caractère à modifier
    %OUT    path : Chaine de carcatère modifié
"""
def autocompletion_path(path):
    path = 'Json/' + path + '.json'
    return path

"""
Fonction dict_to_tab
%       Fonction qui renvoie le tableau équivalent à un dictionnaire
        donné. Selon le motif suivant :
        Pour chaque itération on aura une valeur renseignée, celle-ci
        vaudra une valeur qu'on peut trouver à dict['index'].
        On change de dict['index'][indice] quand on a parcouru dict['div']
        valeur.
        Exemple : Si on a un dictionnaire de ce type :
        {
            "div":3,
            "index":
            {
                'A',
                'B',
                'C'
            }
        } ainsi qu'"iter" valant 9
        Cette fonction nous renverra :
        ['A', 'A', 'A', 'B', 'B', 'B', 'C', 'C', 'C']
%IN     dict : Dictionnaire à transformer en tableau,
        iter : Entier représentant le nombre d'initialisation
%OUT    tab : Tableau de valeur
"""
def dict_to_tab(dict, iter):
    tab = []
    save = 1
    indice_tab = 0
    for indice_dict in range(iter):
        if indice_dict - save == dict['div']:
            indice_tab = indice_tab + 1
        tab.append(dict['index'][indice_tab])
    return tab