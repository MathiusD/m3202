import random
"""
    Fonction calcul_stat
    %       Fonction qui execute un dictionnaire de donnée avec une profondeur souhaitée
            et nous retourne un dictionnaire indiquant pour chaque état le nombre de fois
            qu'il a été pris ainsi que le détail de l'execution
    %IN     data_exercice : Le dictionnaire de données de référence,
            iterate : la pronfondeur d'éexecution
    %OUT    result : le dictionnaire de données
"""
def calcul_stat(data_exercice, iterate):
    result = {}
    result['detail'] = []
    result['final'] = {}
    for data in data_exercice['var']:
        result[data] = result['final'][data] = 0
    for execut in range(data_exercice['iter']):
        result['detail'].append({})
        result['detail'][execut]['act_value'] = data_exercice['init'][execut]
        result['detail'][execut]['hist'] = []
        result['detail'][execut]['hist'].append(data_exercice['init'][execut])
        for iterat in range(iterate):
            maxi = data_exercice['proba'][result['detail'][execut]['act_value']]['divi']
            prob = random.randint(1, maxi)
            parcour = 1
            indice = 0
            found = False
            while parcour < maxi and found == False:
                borne_sup = parcour + data_exercice['proba'][result['detail'][execut]['act_value']][data_exercice['var'][indice]]
                if (parcour <= prob and prob < borne_sup):
                    result['detail'][execut]['act_value'] = data_exercice['var'][indice]
                    result['detail'][execut]['hist'].append(data_exercice['var'][indice])
                    result[result['detail'][execut]['act_value']] = result[result['detail'][execut]['act_value']] + 1
                    if iterat == (iterate - 1):
                        result['final'][result['detail'][execut]['act_value']] = result['final'][result['detail'][execut]['act_value']] + 1
                    found = True
                parcour = borne_sup
                indice = indice + 1
    return result