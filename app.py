#!/usr/bin/python
# -*- coding: UTF8 -*-
from Json.own_json import *
from probs.probs import *
from stats import *
import argparse
import os
parser = argparse.ArgumentParser(description="Calcul des probabilités selon le sujet")
parser.add_argument("exercice", help="CX où X est le numéro de l'exercice")
parser.add_argument("-I", "--iter", help="Nombre de \"tours\" pendant lesquels on effectue les calculs (Default = 5)", type=int, default=5)
parser.add_argument("-d", "--debug", help="Option de Débug", action="store_true")
parsegroup2 = parser.add_mutually_exclusive_group()
parsegroup2.add_argument("-s", "--stats", help="Calculer uniquement les statistiques", action="store_true")
parsegroup2.add_argument("-p", "--probas", help="Calculer uniquement les probabilités", action="store_true")
parsegroup2.add_argument("-a", "--all", help="Calculer les probabilités et les statistiques", action="store_true", default=True)
parsegroup1 = parser.add_mutually_exclusive_group()
parsegroup1.add_argument("-v", "--verbose", help="Donner le plus d'informations possible", action="store_true")
parsegroup1.add_argument("-q", "--quiet", help="Donner le moins d'informations possible", action="store_true")
args = parser.parse_args()
exercice = args.exercice
if (args.verbose):
    print(f"L'argument {exercice=} est correct")
    print("Lancement du traitement des données")
if not args.stats:
    fileContent = read_and_verify_json_mod(args.exercice, False)
    calcProbs(fileContent, args.iter, verbose=args.verbose, quiet=args.quiet)
    print(" ")
if not args.probas:
    fileContent = read_and_verify_json_mod(args.exercice, True)
    stat_affich(fileContent, verbose=args.verbose, profo=args.iter, debug=args.debug)
if args.debug:
    print(read_and_verify_json_mod(args.exercice, True))