def calcProbs(params, iter, /, verbose=False, quiet=False):
    iter += 1
    nbVar = len(params["var"])
    nbSortie = nbVar**iter
    if (verbose):
        print(f"Nombre d'issues possibles : {nbSortie}")
    for init in params['init']['index']:
        tabRes = [0 for i in range(nbVar)]
        parcours = [init for z in range(iter)]
        for j in range(nbSortie):
            z = iter-1
            while z >= 0:
                indexR = params["var"].index(parcours[z])
                parcours[z] = params["var"][indexR+1 if indexR < nbVar-1 else 0]
                if parcours[z] != init:
                    z = -1
                else:
                    z -= 1
            index = params["var"].index(parcours[iter-1])
            brancheProba = 1
            for k in range(iter - 1):
                brancheProba *= params["proba"][parcours[k]][parcours[k+1]] / params["proba"][parcours[k]]["divi"]
            tabRes[index] += brancheProba
        if verbose:
            print(f"En commençant à {init} :")
        elif not quiet:
            print(f"Début : {init}\nFins :")
        for i in range(len(tabRes)):
            tabRes[i] /= nbVar
            if verbose:
                print(f"On fini à \"{params['var'][i]}\" avec {tabRes[i]} de probabilité")
            elif not quiet:
                print(f"\t{params['var'][i]} : {tabRes[i]}")
