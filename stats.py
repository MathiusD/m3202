from Stats.stats import calcul_stat
from Json.own_json import read_and_verify_json_mod

def stat_affich(fich, verbose=False, profo=10, iter_max=100, debug=False):
    if fich == None:
        print("Fichier non trouvé")
    else:
        out_f = []
        for i in range(iter_max):
            out_f.append(calcul_stat(fich, profo))
        if verbose:
            out_fv = {}
            for i in range(iter_max):
                for j in range(fich['iter']):
                    name = ""
                    first_index = False
                    for row in out_f[i]['detail'][j]['hist']:
                        if first_index == True:
                            name = name + ":"
                        name = name + str(row)
                        if first_index == False:
                            first_index = True
                    out_fv[name] = {}
                    for data in fich['var']:
                        out_fv[name][data] = {}
                        out_fv[name][data]['data'] = 0
                        out_fv[name][data]['nb'] = 0
            for i in range(iter_max):
                for j in range(fich['iter']):
                    name = ""
                    first_index = False
                    for row in out_f[i]['detail'][j]['hist']:
                        if first_index == True:
                            name = name + ":"
                        name = name + str(row)
                        if first_index == False:
                            first_index = True
                    for data in fich['var']:
                        out_fv[name][data]['data'] = out_fv[name][data]['data'] + out_f[i]['final'][data]
                        out_fv[name][data]['nb'] = out_fv[name][data]['nb'] + 1
            for row in out_fv:
                print("Execution avec " + row + ":")
                for rew in out_fv[row]:
                    print(str(rew)+" : "+str(out_fv[row][rew]['data']/out_fv[row][rew]['nb'])+"\n")
            if debug:
                print(out_f)
        else:
            out_ff = {}
            for data in fich['var']:
                out_ff[data] = 0
                for row in out_f:
                    out_ff[data] = out_ff[data] + row['final'][data]
                out_ff[data] = out_ff[data]/iter_max
            print("Sur des executions de profondeur de "+str(profo)+" et "+str(fich['iter']*iter_max)+" executions on a :\n")
            for row in out_ff:
                print(row+" : "+str(out_ff[row]/fich['iter'])+"\n")
            if debug:
                print(out_ff)